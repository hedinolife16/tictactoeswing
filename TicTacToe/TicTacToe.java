package TicTacToe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
public final class TicTacToe extends JFrame {

	/**
	 * @author Hedi MLIKA
	 */
	private static final long serialVersionUID = 1L;
	private static final int  version;
	private static final int  defaultWindowSize;		
	private static final int  maximumDimension;

	private static final int  defaultDimension; 		
	private static final User defaultFirstUser;	

	private JPanel contents;
	private JPanel boardPanel;

	private final int  dimension;		
	private final User firstUser;

	private User currentUser; 
	private User[][] board;  
	private int countPlays;
	private Direction winDirection;

	static {
		version 		  = 0;		
		defaultWindowSize = 1000;	
		maximumDimension  = 9;		
		defaultDimension  = 3;		
		defaultFirstUser  = User.X; 
	}

	public static void main(String[] args) {
		if (args.length == 2) {
			play(args[0], args[1]);
		}

		else if (args.length == 1) {
			if (args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
				System.out.println("Usage: java -jar TicTacToe.jar <first user (O/X)> <board dimension>");
			}
			else {
				play(args[0]);
			}
		}

		else {
			play();
		}

	}


	public TicTacToe(String argFirstUser, String argDimension) {
		System.out.println("Starting Tic Tac Toe...");

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}

		setTitle("Tic Tac Toe" + " v" + version);   
		setIconImage(Toolkit.getDefaultToolkit().getImage(TicTacToe.class.getResource("resources/tic-tac-toe-icons.png")));  
		setSize(defaultWindowSize, defaultWindowSize);
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		contents = new JPanel();
		contents.setBorder(new EmptyBorder(5, 5, 5, 5));
		contents.setLayout(new BorderLayout(0, 0));
		setContentPane(contents);

		int dimensionToSet;
		try {
			dimensionToSet = Integer.parseInt(argDimension);
			if (dimensionToSet > maximumDimension || dimensionToSet < 2) throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			System.err.println("Invalid or null value for dimension. Default value " + defaultDimension + " will be used");
			dimensionToSet = defaultDimension;
		}
		dimension = dimensionToSet;


		if (argFirstUser.equalsIgnoreCase("x") || argFirstUser.equalsIgnoreCase("o"))    
			firstUser = User.valueOf(argFirstUser);
		else {
			System.err.println("Invalid or null value for first user. Default value " + defaultFirstUser + " will be used");
			firstUser = defaultFirstUser;
		}

		currentUser = firstUser;
		countPlays  = 0;
		//initialTime = System.currentTimeMillis();
		board = new User[dimension][dimension];

		createBoard();

	}

	public TicTacToe() {
		this("", "");   
	}

	public TicTacToe(String argFirstUser) {
		this(argFirstUser, "");  
	}

	private void createBoard() {

		boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(dimension,dimension));

		for (int i = 0; i < dimension; i++) {				// lignes
			for (int j = 0; j < dimension; j++) {			// colonnes
				JToggleButton b = new JToggleButton();

				final Integer iForActionListener = Integer.valueOf(i);			
				final Integer jForActionListener = Integer.valueOf(j);

				b.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {

						b.setText(currentUser.toString());
						b.setEnabled(false);
						b.setFont(new Font(b.getFont().getName(), Font.PLAIN, 40));

						setBoardUser(iForActionListener, jForActionListener, currentUser);		

						if (currentUser.equals(User.X)) currentUser = User.O;					
						//else if (currentUser.equals(User.O)) currentUser = User.X;
						else currentUser = User.X;

						countPlays++;

						if (isFinished()) finishGame();
					}

				});

				boardPanel.add(b);
			}
		}

		contents.add(boardPanel, BorderLayout.CENTER);
	}


	private static void play(String argFirstUser, String argDimension) {
		TicTacToe jogo = new TicTacToe(argFirstUser, argDimension);
		jogo.setVisible(true);
	}

	private static void play(String argFirstUser) {
		TicTacToe jogo = new TicTacToe(argFirstUser);
		jogo.setVisible(true);
	}


	private static void play() {
		TicTacToe jogo = new TicTacToe();
		jogo.setVisible(true);
	}

	@SuppressWarnings("unused")
	private void restartGame() {
		dispose();				
		play();
	}


	private void restartGame(String argFirstUser, String argDimension) {
		dispose();			
		play(argFirstUser, argDimension);
	}


	private User getBoardUser(int lin, int col) {
		return board[lin][col];
	}

	private void setBoardUser(int lin, int col, User user) {
		this.board[lin][col] = user;
	}	


	private boolean isFinished() {
		return countPlays == dimension * dimension || searchWin(User.X) || searchWin(User.O);
	}

	private void finishGame() {
		Toolkit.getDefaultToolkit().beep();				
		int option = JOptionPane.showConfirmDialog(		
				getContentPane(), "Fin du jeu. \nResultat: " + getWinner() + "\nVoulez vous recommencer?", "Fin du jeu", JOptionPane.YES_NO_OPTION);    

		if (option == 0) restartGame(firstUser.toString(), String.valueOf(dimension));	
		else disableNonPlayedButtons();			
		/*
		else {
			System.out.println("Closing Tic Tac Toe (fin)");
			System.exit(1);
		}
		 */
	}

	private void disableNonPlayedButtons() {
		Component[] buttons = boardPanel.getComponents();
		for (Component c : buttons) 
			if (c.isEnabled()) c.setEnabled(false);	

	}

	private String getWinner() {

		if (searchWin(User.O)) return User.O.toString() + " a gagne (direction " + winDirection + ")";   
		if (searchWin(User.X)) return User.X.toString() + " a gagne (direction " + winDirection + ")";   
		return "Partie Nulle";  
	}


	private boolean searchWin (User user) {
		for (int i = 0; i < dimension; i++) {		
			for (int j = 0; j < dimension; j++) {			
				for (Direction d: Direction.values()) {
					if (searchWin(user, 0, i, j, d)) {
						winDirection = d;
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean searchWin (User user, int currentScore, int line, int col, Direction direction) {
		return searchWin(user, currentScore, line, col, direction.getIncrementLine(),  direction.getIncrementCol()) == dimension;
	}

	private int searchWin (User user, int currentScore, int line, int col, int incrementLine, int incrementCol) {

		if (getBoardUser(line, col) == null) return 0; 

		if (!getBoardUser(line, col).equals(user)) return 0; 

		if (line + incrementLine < 0 || col + incrementCol < 0 || line + incrementLine > dimension - 1 || col + incrementCol > dimension - 1) return 1;	 


		if (currentScore == dimension) 	return currentScore;	

		return searchWin(user, currentScore, line + incrementLine, col + incrementCol, incrementLine, incrementCol) + 1;

	}
}


enum User {
	X, O;
}


enum Direction {

	Vertical(1, 0), Horizontal(0, 1), Diagonal(1, 1), AntiDiagonal(1, -1) {
		@Override 
		public String toString() {
			return "Diagonal";
		}
	};


	private final int incrementLine, incrementCol;


	Direction (int incrementLine, int incrementCol) {
		this.incrementLine = incrementLine; 
		this.incrementCol  = incrementCol;
	}


	public int getIncrementLine() {
		return incrementLine;
	}


	public int getIncrementCol() {
		return incrementCol;
	}

}
